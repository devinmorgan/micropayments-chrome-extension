import React, { useState } from 'react';
import ReactHotkeys, { OnKeyFun } from 'react-hot-keys';
import { useSelector } from 'react-redux';

import { HotkeysEvent } from 'hotkeys-js';
import styled, { ThemeProvider } from 'styled-components/macro';

import { useAppDispatch } from 'state/_shared/configureStore';
import { deleteDonation, getDonationsMap } from 'state/donationsSlice/donationsSlice';
import AppInit from 'view/AppInit/AppInit';
import DefaultFavicon from 'view/Landing/DefaultFavicon.svg';

import { GlobalStyle, GlobalTheme } from '_shared/GlobalStyle';
import { prettyFormatDonation } from '_shared/utils';

const PermittedHotKeys = ['alt+shift+d', 'alt+shift+D', 'cmd+shift+d', 'cmd+shift+D'];

const LandingApp: React.FC = () => {
    const dispatch = useAppDispatch();
    const donationsMap = useSelector(getDonationsMap);
    const descendingByValueDonationMaps = Object.values(donationsMap).sort(
        (don1, don2) => don2.cumulativeDonationAmount - don1.cumulativeDonationAmount,
    );

    const [showDeleteButtons, setShowDeleteButtons] = useState<boolean>(false);

    const handleHotKeyPressed: OnKeyFun = (shortcut: string, evn: KeyboardEvent, handle: HotkeysEvent) => {
        switch (shortcut) {
            case 'alt+shift+d':
            case 'alt+shift+D':
            case 'cmd+shift+d':
            case 'cmd+shift+D': {
                setShowDeleteButtons(true);
                return;
            }
            default: {
                // no hotkey pressed so do nothing
                return;
            }
        }
    };
    const handleDeleteDonation: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        if (event.target) {
            const t = event.target as HTMLButtonElement;
            const url = t.getAttribute('data-url');
            if (url) {
                dispatch(deleteDonation({ webpageUrl: url }));
            }
        }
    };

    const handleHotKeyUp: OnKeyFun = (shortcut: string, evn: KeyboardEvent, handle: HotkeysEvent) => {
        switch (shortcut) {
            case 'alt+shift+d':
            case 'alt+shift+D':
            case 'cmd+shift+d':
            case 'cmd+shift+D': {
                setShowDeleteButtons(false);
                return;
            }
            default: {
                // no hotkey pressed so do nothing
                return;
            }
        }
    };

    const [showLocalDataPrintOut, setShowLocalDataPrintOut] = useState<boolean>(false);
    const handleViewLocalData: React.MouseEventHandler<HTMLButtonElement> = () => {
        setShowLocalDataPrintOut(!showLocalDataPrintOut);
    };

    return (
        <ThemeProvider theme={GlobalTheme}>
            <GlobalStyle />
            <AppInit />
            <ReactHotkeys
                keyName={PermittedHotKeys.join(',')}
                onKeyDown={handleHotKeyPressed}
                onKeyUp={handleHotKeyUp}
            />
            <Container>
                <ViewLocalDataButton onClick={handleViewLocalData}>Share data with Devin</ViewLocalDataButton>
                <MainColumn>
                    <LandingTitle>Donations</LandingTitle>
                    <SummaryList>
                        {descendingByValueDonationMaps.map(({ cumulativeDonationAmount, url, title, favIconUrl }) => {
                            return (
                                <SummaryCard key={url}>
                                    <WebsiteLogo src={favIconUrl || DefaultFavicon} alt={'Website Logo'} />
                                    <DetailsContainer>
                                        <PageTitle>
                                            <a href={url} target="_blank" rel="noopener noreferrer">
                                                {title || url}
                                            </a>
                                        </PageTitle>
                                        <Donation>{prettyFormatDonation(cumulativeDonationAmount)}</Donation>
                                        <DeleteDonation
                                            isVisible={showDeleteButtons}
                                            onClick={handleDeleteDonation}
                                            data-url={url}
                                        >
                                            Delete
                                        </DeleteDonation>
                                    </DetailsContainer>
                                </SummaryCard>
                            );
                        })}
                    </SummaryList>
                    {showLocalDataPrintOut && (
                        <LocalDataPrintOut>
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Donation Amount</th>
                                    <th>Url</th>
                                    <th>Time of Last Donation</th>
                                </tr>
                            </thead>
                            <tbody>
                                {descendingByValueDonationMaps.map(
                                    ({ cumulativeDonationAmount, url, title, donationTimestamps }) => {
                                        return (
                                            <tr key={url}>
                                                <td>{title}</td>
                                                <td>{prettyFormatDonation(cumulativeDonationAmount)}</td>
                                                <td>{url}</td>
                                                <td>{donationTimestamps[0]}</td>
                                            </tr>
                                        );
                                    },
                                )}
                            </tbody>
                        </LocalDataPrintOut>
                    )}
                </MainColumn>
            </Container>
        </ThemeProvider>
    );
};

export default LandingApp;

const Container = styled.div`
    background-color: #f7f6f3;
    min-height: calc(100vh - 10px);
    padding: 10px 0 0 0;
`;

const MainColumn = styled.div`
    width: 600px;
    margin: 0 auto;
`;

const LandingTitle = styled.h1`
    color: #6f6f6f;
    margin: 0 0 1em 0;
    text-align: center;
`;

const SummaryList = styled.ul`
    padding: 0;
`;

const SummaryCard = styled.li`
    height: 40px;
    list-style-type: none;
    margin: 0 0 5px 0;
`;

const WebsiteLogo = styled.img`
    height: 40px;
    width: 40px;
    margin: 0 5px 0 0;
    float: left;
`;
const DetailsContainer = styled.div`
    height: 100%;
    float: left;
`;
const PageTitle = styled.h3`
    font-size: 16px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    width: 535px;
    margin: 0 0 5px 0;

    & > a {
        color: #6d6d6d;
        text-decoration: none;
    }
`;
const Donation = styled.h4`
    font-size: 12px;
    font-weight: normal;
    float: left;
`;

const DeleteDonation = styled.button<{ isVisible: boolean }>`
    display: ${({ isVisible }): string => (isVisible ? 'block' : 'none')};
    float: right;
    border: 0;
    background-color: #f7f6f3;
    color: #9e9e9e;
    cursor: pointer;
`;

const ViewLocalDataButton = styled.button`
    padding: 5px 20px;
    border: 1px solid #87cefa;
    background-color: transparent;
    color: #87cefa;
    float: right;
    border-radius: 5px;
    margin: 0 20px 0 0;
    opacity: 30%;
    &:hover {
        background-color: #87cefa;
        color: #272727;
    }
    &:focus {
        outline: 0;
    }
`;

const LocalDataPrintOut = styled.table``;
