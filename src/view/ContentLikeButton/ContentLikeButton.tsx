import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

import { DONATION_INCREMENT } from 'state/donationsSlice/static';
// import LikeCount from 'view/ContentLikeButton/LikeCount';
import LikeButton from 'view/LikeButton/LikeButton';
import LikeButtonEffects from 'view/LikeButton/LikeButtonEffects';

const WIDGET_SIZE = 48 * 3;

const ContentLikeButton: React.FC<{
    cumulativeDonationAmount: number;
    handleOnLike: React.MouseEventHandler<HTMLButtonElement>;
    handleOnUnlike: React.MouseEventHandler<HTMLButtonElement>;
}> = ({ cumulativeDonationAmount, handleOnLike, handleOnUnlike }) => {
    const numberOfTimesDonated = cumulativeDonationAmount / DONATION_INCREMENT;

    return (
        <Wrapper>
            <LikeButtonEffects size={WIDGET_SIZE} numberOfTimesDonated={numberOfTimesDonated} />
            <LikeButton
                size={WIDGET_SIZE}
                numOfMyLikes={numberOfTimesDonated}
                onLike={handleOnLike}
                onUnlike={handleOnUnlike}
            />
            {/*TODO: uncomment this and use it later*/}
            {/*{typeof numOfLikes === 'number' && <LikeCount numOfMyLikes={numOfMyLikes} numOfLikes={numOfLikes} />}*/}
        </Wrapper>
    );
};

ContentLikeButton.propTypes = {
    cumulativeDonationAmount: PropTypes.number.isRequired,
    handleOnLike: PropTypes.func.isRequired,
    handleOnUnlike: PropTypes.func.isRequired,
};

export default ContentLikeButton;

const Wrapper = styled.div`
    position: relative;
    margin: 0 auto;
    width: ${WIDGET_SIZE}px;
    height: ${WIDGET_SIZE}px;
`;
