import React from 'react';
import { useSpring, animated } from 'react-spring';

import PropTypes from 'prop-types';
import styled from 'styled-components';

const LikeCount: React.FC<{ numOfMyLikes: number; numOfLikes: number }> = ({ numOfMyLikes, numOfLikes }) => {
    const hasMounted = React.useRef<boolean>(false);
    const [label, setLabel] = React.useState<string | null>(null);
    const cachedNumOfMyLikes = React.useRef<number>(numOfMyLikes);

    React.useEffect(() => {
        hasMounted.current = true;
    }, []);

    React.useEffect(() => {
        if (!hasMounted.current) {
            return;
        }

        const diff = numOfMyLikes - cachedNumOfMyLikes.current;

        cachedNumOfMyLikes.current = numOfMyLikes;

        if (diff > 0) {
            setLabel('+$0.05');
        } else if (diff < 0) {
            setLabel('-$0.05');
        }
    }, [numOfMyLikes, hasMounted.current]);

    const style = useSpring({
        from: {
            transform: `translateY(0px)`,
            opacity: 1,
        },
        to: {
            transform: `translateY(-12px)`,
            opacity: 0,
        },
        config: {
            tension: 100,
            friction: 40,
        },
        reset: true,
    });

    return (
        <Wrapper hasBeenLikedByUser={numOfMyLikes > 0}>
            <InnerWrapper>
                {numOfLikes}
                <LabelWrapper>
                    <Label style={style}>{label}</Label>
                </LabelWrapper>
            </InnerWrapper>
        </Wrapper>
    );
};

LikeCount.propTypes = {
    numOfMyLikes: PropTypes.number.isRequired,
    numOfLikes: PropTypes.number.isRequired,
};

export default React.memo(LikeCount);

const Wrapper = styled.div<{ hasBeenLikedByUser: boolean }>`
    padding-left: 22px;
    font-size: 16px;
    min-width: 75px;
    font-weight: var(--font-weight-medium);
    color: ${(p): string => (p.hasBeenLikedByUser ? 'hsl(333deg, 100%, 52%)' : 'var(--color-gray-700)')};
`;

const InnerWrapper = styled.span`
    position: relative;
    display: inline-block;
`;

const LabelWrapper = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    transform: translateX(calc(100% + 4px));
    pointer-events: none;
`;

const Label = styled(animated.div)`
    font-size: 11px;
    color: var(--color-gray-500);
`;
