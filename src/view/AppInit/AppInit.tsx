import React, { useEffect } from 'react';

import { useAppDispatch } from 'state/_shared/configureStore';
import { fetchActiveTabData, loadDonationsSliceFromChromeStorage } from 'state/donationsSlice/donationsSlice';

const AppInit: React.FC = () => {
    const dispatch = useAppDispatch();
    const handleLoadDonationsSliceFromChromeStorageOnInit = useEffect(() => {
        dispatch(loadDonationsSliceFromChromeStorage());
        dispatch(fetchActiveTabData());
    }, [dispatch]);
    return <div />;
};

export default AppInit;
