import React, { useRef } from 'react';
import ReactHotkeys, { OnKeyFun } from 'react-hot-keys';
import { useSelector } from 'react-redux';

import { HotkeysEvent } from 'hotkeys-js';
import get from 'lodash/get';
import styled, { ThemeProvider } from 'styled-components/macro';

import { useAppDispatch } from 'state/_shared/configureStore';
import {
    getCurrentWebsiteDonationData,
    donateToWebpage,
    getCumulativeDonationsSum,
    undoDonateToWebpage,
} from 'state/donationsSlice/donationsSlice';
import AppInit from 'view/AppInit/AppInit';
import ContentLikeButton from 'view/ContentLikeButton/ContentLikeButton';
import ExternalLinkIcon from 'view/Popup/ExternalLinkIcon.png';
import PlusIcon from 'view/Popup/PlusIcon.svg';

import { GlobalStyle, GlobalTheme } from '_shared/GlobalStyle';
import { getLinkForWebApp, PluginRoutes, WebAppRoutes } from '_shared/routes';
import { prettyFormatDonation, throttle } from '_shared/utils';

const PermittedHotKeys = ['D', 'd', 'down', 'up'];

const PopupApp: React.FC = () => {
    const dispatch = useAppDispatch();
    const incrementDonationButtonRef = useRef<HTMLButtonElement>(null);
    const websiteDonationData = useSelector(getCurrentWebsiteDonationData);
    const cumulativeDonationAmount = get(websiteDonationData, ['cumulativeDonationAmount'], 0);
    const cumulativeDonationsSum = useSelector(getCumulativeDonationsSum);

    const handleIncrementDonationClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        dispatch(donateToWebpage());
        event.currentTarget.blur();
    };
    const handleOnContextMenu: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        event.preventDefault();
        dispatch(undoDonateToWebpage());
        event.currentTarget.blur();
    };

    const incrementDonationHotKeyPressed = throttle((): void => {
        // NOTE: we use throttle here to avoid a race condition with
        // multiple button.blur()s firing at sooner than the 50 ms.
        if (incrementDonationButtonRef.current) {
            // Since a hotkey is not a MouseEvent, the IncrementDonationButton
            // never gets pressed (never enters the focussed state). Because it
            // never enters the focussed state, the CSS that makes the button
            // look like it is getting 'pressed' never executes. This code emulates
            // the button pressing behavior each time a hotkey is pressed.
            const button = incrementDonationButtonRef.current;
            button.focus();
            dispatch(donateToWebpage());
            // NOTE: this hack is necessary because without it the button
            // wouldn't stay depressed long enough to look like it had moved.
            setTimeout(() => button.blur(), 50);
        }
    }, 100);

    const decrementDonationHotKeyPressed = (): void => {
        dispatch(undoDonateToWebpage());
    };

    const handleHotKeyPressed: OnKeyFun = (shortcut: string, evn: KeyboardEvent, handle: HotkeysEvent) => {
        switch (shortcut) {
            case 'up':
            case 'D':
            case 'd': {
                incrementDonationHotKeyPressed();
                return;
            }
            case 'down': {
                decrementDonationHotKeyPressed();
                return;
            }
            default: {
                // no hotkey pressed so do nothing
                return;
            }
        }
    };
    return (
        <ThemeProvider theme={GlobalTheme}>
            <GlobalStyle />
            <AppInit />
            <ReactHotkeys keyName={PermittedHotKeys.join(',')} onKeyDown={handleHotKeyPressed} />
            <PopupContainer>
                <CheckOutLeaderBoard
                    href={getLinkForWebApp(WebAppRoutes.LEADER_BOARD)}
                    target={'_blank'}
                    rel={'noopener noreferrer'}
                >
                    Visit the Leaderboard
                </CheckOutLeaderBoard>
                <ContentLikeButton
                    cumulativeDonationAmount={cumulativeDonationAmount}
                    handleOnLike={incrementDonationHotKeyPressed}
                    handleOnUnlike={decrementDonationHotKeyPressed}
                />
                <CurrentPageWrapper>
                    <CurrentPageDonationTotal>
                        {prettyFormatDonation(cumulativeDonationAmount)}
                    </CurrentPageDonationTotal>
                    <IncrementDonationButton
                        onClick={handleIncrementDonationClick}
                        onContextMenu={handleOnContextMenu}
                        ref={incrementDonationButtonRef}
                    >
                        <span />
                    </IncrementDonationButton>
                </CurrentPageWrapper>
                <TotalDonations>
                    So far,{' '}
                    <a href={PluginRoutes.ALL_DONATIONS} target="_blank" rel="noopener noreferrer">
                        you&apos;ve donated
                    </a>{' '}
                    a total of <b>{prettyFormatDonation(cumulativeDonationsSum)}</b>
                </TotalDonations>
            </PopupContainer>
        </ThemeProvider>
    );
};

export default PopupApp;

const PopupContainer = styled.div`
    background-color: #f7f6f3;
    width: 300px;
    padding: 80px 0 5px 0;
`;

const CurrentPageWrapper = styled.div`
    margin: 0 auto;
    width: 60px;
    position: relative;
    bottom: 10px;
`;

const CurrentPageDonationTotal = styled.h1`
    color: #a6a6a6;
    margin: 0 0 10px;
    position: relative;
    right: 5px;
`;

const IncrementDonationButton = styled.button`
    width: 60px;
    height: 30px;
    border: 0;
    border-radius: 5px;
    background-color: #00dab9;
    box-shadow: 0 4px 0 0 #00ae94;

    &:hover {
        background-color: #00c7a9;
        cursor: pointer;
    }

    &:focus {
        position: relative;
        top: 4px;
        box-shadow: 0 0 0 0 #00ae94;
        outline: none;
    }

    & > span {
        display: block;
        background-image: url('${PlusIcon}');
        background-repeat: no-repeat;
        background-position: center;
        background-size: 15px;
        width: 100%;
        height: 100%;
    }
`;

const TotalDonations = styled.p`
    color: #a6a6a6;
    font-size: 12px;
    text-align: center;
    margin: 20px 0 0 0;

    & > a {
        color: #a6a6a6;
    }
`;

const CheckOutLeaderBoard = styled.a`
    color: #a6a6a6;
    font-size: 12px;
    cursor: pointer;
    position: absolute;
    top: 10px;
    right: 5px;

    &:after {
        background-image: url('${ExternalLinkIcon}');
        background-size: contain;
        background-position: center;
        content: ' ';
        display: inline-block;
        width: 15px;
        height: 15px;
        opacity: 0.5;
        margin: 0 5px 0 5px;
        position: relative;
        top: 1px;
    }
`;
