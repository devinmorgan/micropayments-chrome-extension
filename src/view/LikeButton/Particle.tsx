import React from 'react';
import { useSpring, animated } from 'react-spring';

import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components/macro';

import { Sprite } from 'view/ConfettiGeyser/default-sprites';

export interface ParticleProps {
    sprite: Sprite;
    angleInRads: number;
    distance: number;
    rotation: number;
    springConfig: {
        tension: number;
        friction: number;
        mass: number;
    };
}

const ParticleComponent: React.FC<ParticleProps> = ({ angleInRads, distance, sprite, rotation, springConfig }) => {
    const fromX = Math.cos(angleInRads) * (distance * 0.75);
    const fromY = Math.sin(angleInRads) * (distance * 0.75);
    const toX = Math.cos(angleInRads) * distance;
    const toY = Math.sin(angleInRads) * distance;

    const style = useSpring({
        from: {
            transform: `translate(${fromX}px, ${fromY}px) rotate(0deg)`,
        },
        to: {
            transform: `translate(${toX}px, ${toY}px) rotate(${rotation}deg)`,
        },
        config: springConfig,
    });
    return <Image src={sprite.src} style={{ width: sprite.width / 2, height: sprite.height / 2, ...style }} />;
};

ParticleComponent.propTypes = {
    sprite: PropTypes.shape({
        src: PropTypes.string.isRequired,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        airFrictionMultiplier: PropTypes.number.isRequired,
    }).isRequired,
    angleInRads: PropTypes.number.isRequired,
    distance: PropTypes.number.isRequired,
    rotation: PropTypes.number.isRequired,
    springConfig: PropTypes.shape({
        tension: PropTypes.number.isRequired,
        friction: PropTypes.number.isRequired,
        mass: PropTypes.number.isRequired,
    }).isRequired,
};

export default React.memo(ParticleComponent);

const fadeOut = keyframes`
  from {
    opacity: 1;
  }
  to {
    opacity: 0;
  }
`;

const Image = styled(animated.img)`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    transform-origin: center center;
    animation: ${fadeOut} 750ms 200ms both;
`;
