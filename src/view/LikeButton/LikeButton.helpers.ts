import {
    clamp,
    getDistanceBetweenPoints,
    convertRadiansToDegrees,
    BoundingBox,
    Point,
    NullablePoint,
    isPoint,
} from '_shared/utils';

const AREA_OF_EFFECT_MULTIPLIER = 1; // original value was 2

export const getIsWithinRadius = (
    size: number,
    svgBoundingBox: BoundingBox | null,
    cursorPoint: NullablePoint,
): boolean => {
    if (!svgBoundingBox || !isPoint(cursorPoint)) {
        return false;
    }

    const areaOfEffectRadius = size * AREA_OF_EFFECT_MULTIPLIER;

    const headCenterPoint = {
        x: svgBoundingBox.left + svgBoundingBox.width / 2,
        y: svgBoundingBox.top + svgBoundingBox.height / 2,
    };

    const distanceToHead = getDistanceBetweenPoints(cursorPoint, headCenterPoint);

    return distanceToHead <= areaOfEffectRadius;
};

export const getBodyRotation = (
    size: number,
    svgBoundingBox: BoundingBox | null,
    cursorPoint: NullablePoint,
): number => {
    if (!svgBoundingBox || !isPoint(cursorPoint)) {
        return 0;
    }

    const areaOfEffectRadius = size * AREA_OF_EFFECT_MULTIPLIER;

    const headCenterPoint = {
        x: svgBoundingBox.left + svgBoundingBox.width / 2,
        y: svgBoundingBox.top + svgBoundingBox.height / 2,
    };

    const distanceToHead = getDistanceBetweenPoints(cursorPoint, headCenterPoint);

    if (distanceToHead > areaOfEffectRadius) {
        return 0;
    }

    const deltaX = headCenterPoint.x - cursorPoint.x;
    const deltaY = headCenterPoint.y - cursorPoint.y;

    const angleInRads = Math.atan2(deltaY, deltaX);
    const angleInDegrees = 180 + convertRadiansToDegrees(angleInRads);

    const rotationMax = 10;

    let a, b;
    if (angleInDegrees < 180) {
        a = (rotationMax / 90) * -1;
        b = rotationMax;
    } else {
        a = rotationMax / 90;
        b = rotationMax * -3;
    }

    return a * angleInDegrees + b;
};

export const getEyeTranslation = (size: number, svgBoundingBox: BoundingBox, cursorPoint: NullablePoint): Point => {
    const NoTranslation = { x: 0, y: 0 };
    if (!svgBoundingBox || !isPoint(cursorPoint)) {
        return NoTranslation;
    }

    const areaOfEffectRadius = size * AREA_OF_EFFECT_MULTIPLIER;

    const headCenterPoint = {
        x: svgBoundingBox.left + svgBoundingBox.width / 2,
        y: svgBoundingBox.top + svgBoundingBox.height / 2,
    };

    const distanceToHead = getDistanceBetweenPoints(cursorPoint, headCenterPoint);

    if (distanceToHead > areaOfEffectRadius) {
        return NoTranslation;
    }

    const deltaX = cursorPoint.x - headCenterPoint.x;
    const deltaY = cursorPoint.y - headCenterPoint.y;

    const maxDisplacement = size * 0.025;
    const reachMaxAt = areaOfEffectRadius / 2;

    const unboundTranslateX = (deltaX / reachMaxAt) * maxDisplacement;
    const unboundTranslateY = (deltaY / reachMaxAt) * maxDisplacement;

    return {
        x: clamp(unboundTranslateX, -maxDisplacement, maxDisplacement),
        y: clamp(unboundTranslateY, -maxDisplacement, maxDisplacement),
    };
};
