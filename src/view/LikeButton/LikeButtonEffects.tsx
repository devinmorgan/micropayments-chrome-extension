import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

import { TARGET_NUMBER_OF_DONATIONS } from 'state/donationsSlice/static';
import defaultSprites from 'view/ConfettiGeyser/default-sprites';
import ParticleComponent, { ParticleProps } from 'view/LikeButton/Particle';

import useInterval from '_shared/hooks/use-interval.hook';
import { generateId, sample, random, range, normalize } from '_shared/utils';

interface ParticleData extends ParticleProps {
    id: string;
    createdAt: number;
    angle: number;
}

const generateParticles = (num: number, size: number): ParticleData[] => {
    const createdAt = Date.now();

    return range(1, num).map((i) => {
        const [sprite] = sample(defaultSprites);

        // I want to distribute the particles roughly equally across 360 degrees.
        // If we are generating 4 particles, each should be in its own quadrant.
        // For that, I need to figure out the "ratio" of this sprite;
        // 1/4 is 0.25, 2/4 is 0.5, 3/4 is 0.75, 4/4 is 1.
        const angleMin = (i / num) * 360;
        const angleMax = ((i + 1) / num) * 360;

        const angle = random(angleMin, angleMax);
        const angleInRads = (angle * Math.PI) / 180;

        let distance = random(size * 0.5, size * 1.5);
        if (angle > 45 && angle < 135) {
            distance *= 0.5;
        }

        const rotation = random(0, 180);

        const tension = normalize(distance, size * 0.5, size, 150, 300);
        const friction = normalize(distance, size * 0.5, size, 22, 26);
        const mass = normalize(sprite.width, 12, 50, 0.75, 1.5);

        return {
            id: generateId(8),
            createdAt,
            sprite,
            angle,
            angleInRads,
            distance,
            rotation,
            springConfig: {
                tension,
                friction,
                mass,
            },
        };
    });
};

const LikeButtonEffects: React.FC<{
    numberOfTimesDonated: number;
    size: number;
}> = ({ numberOfTimesDonated, size }) => {
    const [particles, setParticles] = React.useState<ParticleData[]>([]);
    const cachedNumOfTimesDonated = React.useRef<number>(numberOfTimesDonated);

    const handleOnlyEmitParticlesWhenClickIsMaxLikes = React.useEffect(() => {
        // NOTE: We want to make sure that the particles are only emmitted when someone is
        // donating additional money and never when they are removing donations, even if
        // after removing money they are still above the target number of donations
        const diff = numberOfTimesDonated - cachedNumOfTimesDonated.current;
        if (diff > 0 && numberOfTimesDonated >= TARGET_NUMBER_OF_DONATIONS) {
            const nextParticles = [...particles, ...generateParticles(9, size)];
            setParticles(nextParticles);
        }

        cachedNumOfTimesDonated.current = numberOfTimesDonated;
    }, [numberOfTimesDonated]);

    // Garbage collection
    useInterval(() => {
        const now = Date.now();
        const freshParticles = particles.filter((particle) => {
            const delta = now - particle.createdAt;

            return delta < 1500;
        });

        if (freshParticles.length < particles.length) {
            setParticles(freshParticles);
        }
    }, 5000);

    return (
        <Wrapper style={{ width: size, height: size }}>
            {particles.map((particle) => (
                <ParticleComponent key={particle.id} {...particle} />
            ))}
        </Wrapper>
    );
};

LikeButtonEffects.propTypes = {
    numberOfTimesDonated: PropTypes.number.isRequired,
    size: PropTypes.number.isRequired,
};

export default LikeButtonEffects;

const Wrapper = styled.div`
    position: absolute;
    z-index: 0;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
`;
