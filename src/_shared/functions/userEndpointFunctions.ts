import { NormalizedUrl, TimeStamp, UserUid } from 'state/donationsSlice/static';

import { functions } from '_shared/firebase';

const _donationsEndpointCallable = functions.httpsCallable('donationEndpoint');

export const createNewDonationFunction = async (body: {
    title: string | null;
    url: NormalizedUrl;
    time: TimeStamp;
    user: UserUid;
}): Promise<void> => {
    await _donationsEndpointCallable({ operation: 'INCREMENT', payload: body });
    return;
};
export const deleteLatestDonationFunction = async (body: { url: NormalizedUrl; user: UserUid }): Promise<void> => {
    await _donationsEndpointCallable({ operation: 'DECREMENT', payload: body });
    return;
};
export const deleteAllDonationsFunction = async (body: { url: NormalizedUrl; user: UserUid }): Promise<void> => {
    console.log('deleteAllDonationsFunction called');
    await _donationsEndpointCallable({ operation: 'DELETE', payload: body });
    return;
};
