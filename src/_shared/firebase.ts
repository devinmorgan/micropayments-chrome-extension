// import * as firebase from 'firebase/app';
import firebase from 'firebase';

// Add the Firebase products that you want to use
// import 'firebase/auth';
// import 'firebase/firestore';
import 'firebase/functions';
// import 'firebase/storage';
// import 'firebase/analytics';

const firebaseConfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    // databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID,
    measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};
firebase.initializeApp(firebaseConfig);

// const _provider = new firebase.auth.GoogleAuthProvider();
// const _storage = firebase.storage();

// const _auth = firebase.auth();
// const emulatorAuthenticationUrl = process.env.REACT_APP_EMULATOR_AUTHENTICATION_URL;
// if (emulatorAuthenticationUrl) {
//     _auth.useEmulator(emulatorAuthenticationUrl);
// }

// const _firestore = firebase.firestore();
// const emulatorFirestoreHost = process.env.REACT_APP_EMULATOR_FIRESTORE_HOST;
// if (emulatorFirestoreHost) {
//     _firestore.settings({
//         host: emulatorFirestoreHost,
//         ssl: false,
//     });
// }

const _functions = firebase.functions();
const emulatorFunctionsUrl = process.env.REACT_APP_EMULATOR_FUNCTIONS_URL;
if (emulatorFunctionsUrl) {
    try {
        const { hostname, port } = new URL(emulatorFunctionsUrl);
        firebase.functions().useEmulator(hostname, parseInt(port));
    } catch (e) {
        console.error('Invalid REACT_APP_EMULATOR_FUNCTIONS_URL format');
        throw e;
    }
}

// export const auth = _auth;
export const functions = _functions;
// export const firestore = _firestore;
// export const provider = _provider;
// export const storage = _storage;
// export const analytics = firebase.default.analytics();
export default firebase;
