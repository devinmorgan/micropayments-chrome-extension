export enum PluginRoutes {
    ALL_DONATIONS = '/landing.html',
    PLUGIN_POPUP = '/popup.html',
}

export enum WebAppRoutes {
    VIEW_ALL_DONATIONS = '/my-donations',
    MAKE_DONATION = '/donate',
    LEADER_BOARD = '/leader-board',
    HOME = '/home',
}

export const getLinkForWebApp = (route: WebAppRoutes): string => {
    return `${process.env.REACT_APP_CLIENT_APP_URL}${route}`;
};
