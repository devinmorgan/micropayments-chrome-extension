import * as React from 'react';

import { BoundingBox, getDistanceBetweenPoints, NullablePoint } from '_shared/utils';

function useMousePositionNearElement({
    boundingBox,
    radius,
}: {
    boundingBox: BoundingBox | null;
    radius: number;
}): NullablePoint {
    const [mousePosition, setMousePosition] = React.useState<NullablePoint>({
        x: null,
        y: null,
    });

    React.useEffect(() => {
        const updateMousePosition = (ev: MouseEvent): void => {
            if (!boundingBox) {
                return;
            }

            const mousePoint = { x: ev.clientX, y: ev.clientY };

            const centerPoint = {
                x: boundingBox.left + boundingBox.width / 2,
                y: boundingBox.top + boundingBox.height / 2,
            };

            const distance = getDistanceBetweenPoints(mousePoint, centerPoint);

            if (distance < radius) {
                setMousePosition(mousePoint);
            }
        };

        window.addEventListener('mousemove', updateMousePosition);

        return (): void => {
            window.removeEventListener('mousemove', updateMousePosition);
        };
    }, [boundingBox, radius]);

    return mousePosition;
}

export default useMousePositionNearElement;
