import React from 'react';

function useInterval<T extends any>(callback: (...args: any[]) => T, delay: number): number | undefined {
    const intervalId = React.useRef<number>();
    const savedCallback = React.useRef(callback);

    React.useEffect(() => {
        savedCallback.current = callback;
    });

    React.useEffect(() => {
        const tick = (): T => savedCallback.current();

        intervalId.current = window.setInterval(tick, delay);

        return (): void => window.clearInterval(intervalId.current);
    }, [delay]);

    return intervalId.current;
}

export default useInterval;
