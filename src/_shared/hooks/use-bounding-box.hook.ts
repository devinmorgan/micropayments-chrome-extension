import React from 'react';

import useForceUpdate from '_shared/hooks/use-force-update.hook';

const useBoundingBox = (): [React.Ref<SVGSVGElement>, DOMRect | null] => {
    // Our `ref` is needed to be passed to the component's `ref` attribute.
    const ref = React.useRef<SVGSVGElement | null>(null);
    // We're using `useRef` for our boundingBox just as an instance variable.
    // Some bit of mutable state that doesn't require re-renders.
    const boundingBox = React.useRef<DOMRect | null>(null);

    const forceUpdate = useForceUpdate();
    React.useEffect(() => {
        if (ref.current) {
            boundingBox.current = ref.current.getBoundingClientRect();
            forceUpdate();
        }
    }, [ref.current]);

    return [ref, boundingBox.current];
};

export default useBoundingBox;
