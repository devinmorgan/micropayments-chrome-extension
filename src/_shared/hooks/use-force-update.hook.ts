import React from 'react';

export default function useForceUpdate(): () => void {
    const [, setState] = React.useState<number | null>(null);

    const forceUpdate = React.useCallback(() => {
        setState(Math.random());
    }, []);

    return forceUpdate;
}
