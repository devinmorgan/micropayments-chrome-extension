import { Action, ThunkAction } from '@reduxjs/toolkit';

import { DonationsSlice } from 'state/donationsSlice/static';

export type RootState = {
    donations: DonationsSlice;
};

export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
