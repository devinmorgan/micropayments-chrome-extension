import { useDispatch } from 'react-redux';

import { configureStore } from '@reduxjs/toolkit';

import { RootState } from 'state/_shared/reduxStatic';
import donationsReducer from 'state/donationsSlice/donationsSlice';

export const store = configureStore<RootState>({
    reducer: {
        donations: donationsReducer,
    },
});

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = (): AppDispatch => useDispatch<AppDispatch>(); // Export a hook that can be reused to resolve types
