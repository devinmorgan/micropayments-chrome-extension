import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import get from 'lodash/get';

import { RootState } from 'state/_shared/reduxStatic';
import {
    DonationsSlice,
    initialState,
    normalizeUrl,
    DONATION_INCREMENT,
    WebpageDonation,
    WebpageDonationMapping,
    TabData,
    NormalizedUrl,
    TimeStamp,
    UserUid,
    Cents,
} from 'state/donationsSlice/static';

import {
    createNewDonationFunction,
    deleteAllDonationsFunction,
    deleteLatestDonationFunction,
} from '_shared/functions/userEndpointFunctions';
import { uuidv4 } from '_shared/utils';

// ============= Thunks ===============

export const fetchActiveTabData = createAsyncThunk<TabData, void>('donations/fetchActiveTabData', (args) => {
    return new Promise((fulfill, reject) => {
        chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
            const [activeTab] = tabs;
            fulfill({
                favIconUrl: activeTab.favIconUrl || null,
                title: activeTab.title || null,
                url: normalizeUrl(activeTab.url),
            });
        });
    });
});

export const saveDonationsSliceToChromeStorage = createAsyncThunk<void, void>(
    'donations/saveAppToChromeStorage',
    async (args, { getState }): Promise<void> => {
        const { donations } = getState() as RootState;
        return new Promise((fulfill) => {
            chrome.storage.sync.set(
                // NOTE: To make life easy, we save data to chrome storage in
                // the same shape as DonationsSlice.
                {
                    donations: {
                        donationsMap: donations.donationsMap,
                        userUid: donations.userUid,
                    },
                },
                () => {
                    fulfill();
                },
            );
        });
    },
);

export const loadDonationsSliceFromChromeStorage = createAsyncThunk<
    Pick<DonationsSlice, 'donationsMap' | 'userUid'>,
    void,
    { dispatch: any }
>('donations/loadDonationsSliceFromChromeStorage', (args, { dispatch }) => {
    return new Promise((fulfill, reject) => {
        chrome.storage.sync.get(['donations'], (result) => {
            fulfill({
                donationsMap: get(result, ['donations', 'donationsMap'], initialState.donationsMap),
                userUid: get(result, ['donations', 'userUid'], uuidv4()),
            });
        });
    });
});

export const donateToWebpage = createAsyncThunk<void, void, { dispatch: any; state: { donations: DonationsSlice } }>(
    'donations/donateToWebpage',
    async (args, { dispatch, getState }) => {
        const { activeTab, userUid } = getState().donations;
        if (activeTab && userUid) {
            const { url, favIconUrl, title } = activeTab;
            const timestamp = Date.now();
            dispatch(incrementWebpageDonation({ title, amount: DONATION_INCREMENT, url, favIconUrl, timestamp }));
            await Promise.all([
                dispatch(saveDonationsSliceToChromeStorage()),
                createNewDonationFunction({ time: timestamp, title, url, user: userUid }),
            ]);
        }
    },
);

export const undoDonateToWebpage = createAsyncThunk<
    void,
    void,
    { dispatch: any; state: { donations: DonationsSlice } }
>('donations/undoDonateToWebpage', async (args, { dispatch, getState }) => {
    const { activeTab, userUid } = getState().donations;
    if (activeTab && userUid) {
        const { url } = activeTab;
        dispatch(decrementWebpageDonation());
        await Promise.all([
            dispatch(saveDonationsSliceToChromeStorage()),
            deleteLatestDonationFunction({ url, user: userUid }),
        ]);
    }
});

export const deleteDonation = createAsyncThunk<
    void,
    { webpageUrl: NormalizedUrl },
    { dispatch: any; state: { donations: DonationsSlice } }
>('donations/deleteDonation', async ({ webpageUrl }, { dispatch, getState }) => {
    const { userUid } = getState().donations;
    if (userUid) {
        dispatch(deleteDonationFromDonationsMap(webpageUrl));
        await Promise.all([
            dispatch(saveDonationsSliceToChromeStorage()),
            deleteAllDonationsFunction({ user: userUid, url: webpageUrl }),
        ]);
    }
});

// ============= Reducer ===============

export const donationsSlice = createSlice({
    name: 'donations',
    initialState,
    reducers: {
        setActiveTab: (state: DonationsSlice, { payload }: PayloadAction<TabData>): void => {
            state.activeTab = payload;
        },
        deleteDonationFromDonationsMap: (
            state: DonationsSlice,
            { payload: url }: PayloadAction<NormalizedUrl>,
        ): DonationsSlice => {
            delete state.donationsMap[url];
            return state;
        },
        incrementWebpageDonation: (
            state: DonationsSlice,
            {
                payload,
            }: PayloadAction<{
                url: NormalizedUrl;
                favIconUrl: string | null;
                title: string | null;
                timestamp: TimeStamp;
                amount: Cents;
            }>,
        ): void => {
            const { url, favIconUrl, title, timestamp, amount } = payload;
            const cumulativeAmount = get(state.donationsMap, [url, 'cumulativeDonationAmount'], 0);
            const donationTimestamps = get(state.donationsMap, [url, 'donationTimestamps'], []);
            state.donationsMap[url] = {
                cumulativeDonationAmount: cumulativeAmount + amount,
                donationTimestamps: [timestamp, ...donationTimestamps],
                url,
                favIconUrl,
                title,
            };
        },
        decrementWebpageDonation: (state: DonationsSlice): DonationsSlice => {
            const { activeTab } = state;
            if (activeTab) {
                const { url, favIconUrl, title } = activeTab;
                const cumulativeAmount = get(state.donationsMap, [url, 'cumulativeDonationAmount'], 0);
                const donationTimestamps = get(state.donationsMap, [url, 'donationTimestamps'], []);
                const newDonationAmount = Math.max(cumulativeAmount - DONATION_INCREMENT, 0);
                if (newDonationAmount === 0) {
                    delete state.donationsMap[url];
                } else {
                    state.donationsMap[url] = {
                        cumulativeDonationAmount: newDonationAmount,
                        // for simplicity, we'll remove the most recent donation timestamp
                        donationTimestamps: donationTimestamps.slice(1),
                        url,
                        favIconUrl,
                        title,
                    };
                }
            }
            return state;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(loadDonationsSliceFromChromeStorage.fulfilled, (state, { payload }): void => {
            state.donationsMap = payload.donationsMap;
            state.userUid = payload.userUid;
        });
        builder.addCase(fetchActiveTabData.fulfilled, (state, { payload }): void => {
            state.activeTab = payload;
        });
    },
});

export default donationsSlice.reducer;

export const {
    incrementWebpageDonation,
    setActiveTab,
    decrementWebpageDonation,
    deleteDonationFromDonationsMap,
} = donationsSlice.actions;

// ============= Selectors ===============

export const getActiveTab = (state: RootState): TabData | null => state.donations.activeTab;
export const getDonationsMap = (state: RootState): WebpageDonationMapping => state.donations.donationsMap;
export const getCurrentWebsiteDonationData = (state: RootState): WebpageDonation | null => {
    const currentUrl = get(state.donations.activeTab, ['url']);
    return currentUrl ? get(state.donations.donationsMap, [currentUrl], null) : null;
};
export const getCumulativeDonationsSum = (state: RootState): number => {
    const { donationsMap } = state.donations;
    return Object.values(donationsMap).reduce((sum, donMap) => sum + donMap.cumulativeDonationAmount, 0);
};
