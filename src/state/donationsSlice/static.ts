// ================= Types =================

import _normalizeUrl from 'normalize-url';

export type Cents = number;
// a url that has been normalized to have a
// trailing '/' and any query params removed
export type NormalizedUrl = string;
export type UserUid = string;
export type DocUid = string;
export type TimeStamp = number;

export interface WebpageDonation {
    cumulativeDonationAmount: Cents;
    donationTimestamps: TimeStamp[];
    url: NormalizedUrl;
    favIconUrl: string | null;
    title: string | null;
}
export interface WebpageDonationMapping {
    [url: string /*NormalizedUrl*/]: WebpageDonation;
}
export interface TabData {
    url: NormalizedUrl;
    favIconUrl: string | null;
    title: string | null;
}
export type DonationsSlice = {
    donationsMap: WebpageDonationMapping;
    activeTab: TabData | null;
    userUid: UserUid | null;
};

// ================= Constants =================

export const DONATION_INCREMENT: Cents = 5;
export const TARGET_NUMBER_OF_DONATIONS = 5;

export const initialState: DonationsSlice = {
    donationsMap: {},
    activeTab: null,
    userUid: null,
};

// ================= Utils =================
export const normalizeUrl = (rawUrl: string | undefined): NormalizedUrl => {
    if (!rawUrl) {
        return 'unknown_url';
    }
    return _normalizeUrl(rawUrl, {
        removeTrailingSlash: false,
    }) as NormalizedUrl;
};
