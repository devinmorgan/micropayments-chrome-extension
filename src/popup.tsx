import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { store } from 'state/_shared/configureStore';
import PopupApp from 'view/Popup/PopupApp';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <PopupApp />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root'),
);
