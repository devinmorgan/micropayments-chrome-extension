import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { store } from 'state/_shared/configureStore';
import LandingApp from 'view/Landing/LandingApp';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <LandingApp />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root'),
);
