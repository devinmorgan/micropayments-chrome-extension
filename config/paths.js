/* eslint-disable @typescript-eslint/no-var-requires,@typescript-eslint/explicit-function-return-type */
'use strict';

const fs = require('fs');
const path = require('path');

const getPublicUrlOrPath = require('react-dev-utils/getPublicUrlOrPath');

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebook/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);

// We use `PUBLIC_URL` environment variable or "homepage" field to infer
// "public path" at which the app is served.
// webpack needs to know it to put the right <script> hrefs into HTML even in
// single-page apps that may serve index.html for nested URLs like /todos/42.
// We can't use a relative path in HTML because we don't want to load something
// like /todos/42/static/js/bundle.7289d.js. We have to know the root.
const publicUrlOrPath = getPublicUrlOrPath(
    process.env.NODE_ENV === 'development',
    require(resolveApp('package.json')).homepage,
    process.env.PUBLIC_URL,
);

const moduleFileExtensions = [
    'web.mjs',
    'mjs',
    'web.js',
    'js',
    'web.ts',
    'ts',
    'web.tsx',
    'tsx',
    'json',
    'web.jsx',
    'jsx',
];

// Resolve file paths in the same order as webpack
const resolveModule = (resolveFn, filePath) => {
    const extension = moduleFileExtensions.find((extension) => fs.existsSync(resolveFn(`${filePath}.${extension}`)));

    if (extension) {
        return resolveFn(`${filePath}.${extension}`);
    }

    return resolveFn(`${filePath}.js`);
};

// config after eject: we're in ./config/
module.exports = {
    dotenv: resolveApp('.env'),
    appPath: resolveApp('.'),
    appBuild: resolveApp('build'),
    appPublic: resolveApp('public'),
    appHtml: resolveApp('public/index.html'),
    appIndexJs: resolveModule(resolveApp, 'src/index'),
    appPackageJson: resolveApp('package.json'),
    appSrc: resolveApp('src'),
    appTsConfig: resolveApp('tsconfig.json'),
    appJsConfig: resolveApp('jsconfig.json'),
    yarnLockFile: resolveApp('yarn.lock'),
    testsSetup: resolveModule(resolveApp, 'src/setupTests'),
    proxySetup: resolveApp('src/setupProxy.js'),
    appNodeModules: resolveApp('node_modules'),
    swSrc: resolveModule(resolveApp, 'src/service-worker'),
    publicUrlOrPath,
    // ~~~~~~~MY_CODE~~~~~~~
    publicUrl: resolveApp('build/'),
    extensionManifest: resolveApp('public/manifest.json'),

    // Background Script Related Paths
    backgroundScriptJsEntry: resolveApp('src/background.js'),
    backgroundScriptJsOutput: resolveApp('build/background.bundle.js'),

    // Content Script Related Paths
    contentScriptJsEntry: resolveApp('src/contentScript.js'),
    contentScriptJsOutput: resolveApp('build/contentScript.bundle.js'),

    // Popup Related Paths
    popupIndexTs: resolveModule(resolveApp, 'src/popup'),
    popupHtmlTemplate: resolveApp('public/popup.html'),
    popupHtmlOutput: resolveApp('build/popup.html'),
    // Landing Related paths
    landingIndexTs: resolveModule(resolveApp, 'src/landing'),
    landingHtmlTemplate: resolveApp('public/landing.html'),
    landingHtmlOutput: resolveApp('build/landing.html'),
};

module.exports.moduleFileExtensions = moduleFileExtensions;
