/* eslint-disable @typescript-eslint/no-var-requires,@typescript-eslint/explicit-function-return-type */
// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

const webpack = require('webpack');

const configFactory = require('../config/webpack.config');
const config = configFactory('development');

webpack(config).watch({}, (err, stats) => {
    if (err) {
        console.error(err);
    }
    console.error(
        stats.toString({
            chunks: false,
            colors: true,
        }),
    );
});
