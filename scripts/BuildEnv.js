/* eslint-disable @typescript-eslint/no-var-requires,@typescript-eslint/explicit-function-return-type */
const fs = require('fs');
const path = require('path');

const commandLineArgs = require('command-line-args');

const PROJECT_ROOT = path.resolve(__dirname, '../');
const ROOT_ENV_FILE_PATH = path.join(PROJECT_ROOT, '.env');

const EXTENSION_DEV_ENV = {
    name: 'extension-dev',
    path: './envs/.extension.dev.env',
};
const EXTENSION_STAGING_ENV = {
    name: 'extension-staging',
    path: './envs/.extension.staging.env',
};
const EXTENSION_PROD_ENV = {
    name: 'extension-prod',
    path: './envs/.extension.prod.env',
};

const ALLOWED_ENVIRONMENTS = [EXTENSION_DEV_ENV, EXTENSION_STAGING_ENV, EXTENSION_PROD_ENV];
const ENVIRONMENT_CL_ARG = 'environment';

const optionDefinitions = [
    {
        name: ENVIRONMENT_CL_ARG,
        alias: 'e',
        type: String,
        defaultOption: EXTENSION_DEV_ENV.name,
    },
];

const deleteRootEnvFile = () => {
    try {
        fs.unlinkSync(ROOT_ENV_FILE_PATH);
    } catch (error) {
        if (error && error.code === 'ENOENT') {
            // file doens't exist
            console.info("File doesn't exist, won't remove it.");
        } else {
            throw error;
        }
    }
};

const getTargetEnvironment = () => {
    const options = commandLineArgs(optionDefinitions);
    const environmentArg = options[ENVIRONMENT_CL_ARG];
    const [envTuple] = ALLOWED_ENVIRONMENTS.filter((env) => env.name === environmentArg);
    if (envTuple) {
        return envTuple;
    }
    throw new Error(`Bad environment type. Must be one of ${ALLOWED_ENVIRONMENTS}`);
};

const createEnvFileForEnvironment = (env) => {
    fs.copyFileSync(path.join(PROJECT_ROOT, env.path), ROOT_ENV_FILE_PATH);
};

if (require.main === module) {
    deleteRootEnvFile();
    const targetEnv = getTargetEnvironment();
    createEnvFileForEnvironment(targetEnv);
}
