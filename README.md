This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Getting Started

1) Install [yarn](https://yarnpkg.com/getting-started/install) - it's Facebook's version of Node Package Manager (NPM)
2) `cd` into your local copy of this repository and run `yarn install`
3) Then run `yarn watch`. This will build the chrome extension and in the `/build` directory.
4) Upload the `/build` folder into your Chrome browser to test the plugin.

## Available Scripts

In the project directory, you can run:

### `yarn run watch` or `yarn watch` or `node scripts/watch.js`

This will build an unoptimized version of the Google Chrome browser extension in the `/build` directory. This is the directly that can be unloaded to run the chrome extension locally

### `yarn run build` or `yarn build` or `node scripts/build.s`

This will build a **production optimized version** of the Google Chrome browser extension in the `/build` directory.Use this command when sharing an official release of the plugin
